<!-- Header inclusion -->
<?php include('pages/includes/header.php'); ?>
<!-- End Header inclusion -->

<!--container-->
<div class="jumbotron jumbotron-odi-hero bg-custom mb-0 jumbotron-custom">
   <div class="jumbotron-overlay ">
      <div class="container text-center">
		<div class="container d-flex h-100">
		    <div class="row justify-content-center align-self-center">
				<h1 class="display-4">So... Let's start the assignment!</h1>
				<p>
					<a href="<?=$url?>pages/assignment.php" class="btn btn-lg btn-light btn-circle my-4 mr-3"><i class="far fa-play-circle"></i> Start the assignment</a>
				</p>
		    </div>
		</div>
      </div>
   </div>
</div>

<!-- Footer inclusion -->
<?php include('pages/includes/footer.php'); ?>
<!-- End Footer inclusion -->