<div class="container mt-5 chartContent animate__animated animate__fadeInUp">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-6 text-center blockChartRequestMinute pb-4">
        	<h5>Requests per minute over the entire time span</h5>
            <div id="chart_div_request_minute"></div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 text-center blockChartHttpMethods pb-4">
        	<h5>Distribution of HTTP methods</h5>
            <div id="chart_div_http_methods"></div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 text-center blockChartHttpAnswerMethods pb-4">
        	<h5>Distribution of HTTP answer codes</h5>
            <div id="chart_div_http_answer_methods"></div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 text-center blockChartSizeAnswer pb-4">
        	<h5>Distribution of the size of the answer of all requests with code 200 and size < 1000B</h5>
            <div id="chart_div_size_answer"></div>
        </div>
    </div>
</div>