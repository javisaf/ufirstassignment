<!-- Header inclusion -->
<?php include('includes/header.php'); ?>
<?php include('../core/functions.php'); ?>
<!-- End Header inclusion -->
<div class="content" style="position: relative; min-height: 100vh;">
	<!-- We import the uploader form firts of all -->
	<?php include('upload_file_view.php'); ?>

	<!-- Now we import the chart area with loaded charts -->
	<?php include('result_charts_view.php'); ?>
</div>
<!-- Footer inclusion -->
<?php include('includes/footer.php'); ?>
<!-- End Footer inclusion -->