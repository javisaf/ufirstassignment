<html>
  <?php $url = 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>
  <!-- We've created this include to add it at top of every page -->
  <head>
  	<title>UFirst Group - Assignment - Javi Safont</title>
  	<link rel="shortcut icon" href="/ufirstassignment/assets/img/favicon.ico" type="image/x-icon">
  	<link rel="icon" href="/ufirstassignment/assets/img/favicon.ico" type="image/x-icon">
  	<meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="/ufirstassignment/assets/css/bootstrap.min.css">
      <link rel="stylesheet" href="/ufirstassignment/assets/css/normalize.css">
      <link rel="stylesheet" href="/ufirstassignment/assets/css/main.css">
      <link rel="stylesheet" href="/ufirstassignment/assets/css/fontawesome/all.css">
      <link rel="stylesheet" href="/ufirstassignment/assets/css/animate.min.css">

  	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;700&display=swap" rel="stylesheet">
  </head>
  
  <body>
    <!-- Navbar starts -->
    <nav class="navbar navbar-expand-md navbar-light bg-light">
       <div class="container">
          <a class="navbar-brand" href="#">
             <h3 class="mb-0"><img src="/ufirstassignment/assets/img/header-brand.svg" class="logo"></h3>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
             <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                   <a class="nav-link p-2" href="/ufirstassignment/">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link p-2" href="#" data-toggle="modal" data-target="#modalHelp">Help</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link p-2" href="https://jsafont.com" target="_blank">My website</a>
                </li>
             </ul>
          </div>
       </div>
    </nav>
    <!-- Navbar ends -->

    <div class="modal fade" id="modalHelp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Do you need help?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
             </div>
             <div class="modal-body">
                <div class="container py-3">
                   <div class="row">
                      <div class="col-12 mx-auto">
                         <div class="accordion" id="faqExample">
                            <div class="card">
                               <div class="card-header p-2" id="headingOne">
                                  <h5 class="mb-0">
                                     <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                     Q: What is the purpose of this assignment?
                                     </button>
                                  </h5>
                               </div>
                               <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faqExample">
                                  <div class="card-body">
                                     <b>Answer:</b> Your task is to import the access logs for the EPA from 1995, restructure the data and provide a graphical analysis of the data.
                                  </div>
                               </div>
                            </div>
                            <div class="card">
                               <div class="card-header p-2" id="headingTwo">
                                  <h5 class="mb-0">
                                     <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                     Q: How does it work?
                                     </button>
                                  </h5>
                               </div>
                               <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                                  <div class="card-body">
                                    The information is processed and stored in JSON in real time. It is developed with <b>HTML, CSS, AJAX and PHP</b>. In addition, <b>Google Charts</b> has been used for charts processing.<br/>
                                    Real-time processing gives the assignment objective a better feeling for a potential end customer.
                                  </div>
                               </div>
                            </div>
                            <div class="card">
                               <div class="card-header p-2" id="headingThree">
                                  <h5 class="mb-0">
                                     <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                     Q. How can I upload a file?
                                     </button>
                                  </h5>
                               </div>
                               <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                                  <div class="card-body">
                                    You can upload the file through the assignment form or you can also select a file previously uploaded to the system.
                                  </div>
                               </div>
                            </div>
                            <div class="card">
                               <div class="card-header p-2" id="headingFour">
                                  <h5 class="mb-0">
                                     <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#headingFour" aria-expanded="false" aria-controls="headingFour">
                                     Q. How can I upload a file via command line?
                                     </button>
                                  </h5>
                               </div>
                               <div id="headingFour" class="collapse" aria-labelledby="headingFour" data-parent="#faqExample">
                                  <div class="card-body">
                                    You can upload the file via command line in the following format:<br/>
                                    <pre>curl -i -F fileUpload=@fileFromYourLocalorRemoteSystem http://51.178.43.68/ufirstassignment/core/uploadFile.php</pre>
                                    Once you uploaded the file, you can click the "Refresh files" link inside the file container to get the last files.
                                  </div>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <!--/row-->
                </div>
             </div>
          </div>
       </div>
    </div>