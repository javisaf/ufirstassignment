<div class="container animate__animated animate__fadeIn">
    <div class="row">
        <div class="col-12">
            <div class="text-center pt-4">
                <h4 class="fw-600">Welcome to the UFirst Group Assignment</h4>
                <h5>Please, first of all upload your file or select an existent file. It will be converted and it will show you the results on the charts.</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="form-container pt-5 areaUploaderContainer">
                <div class="row">
                   <div class="col-lg-12">
                      <div class="p-4 bg-white block-uploader">
                         <img src="/ufirstassignment/assets/img/cloud-computing.png" alt="" width="130" class="d-block mx-auto mb-4">
                         <!-- Custom bootstrap upload file-->
                         <label for="fileUpload" class="file-upload btn btn-info btn-block rounded-pill"><i class="fa fa-upload mr-2"></i>Browse for file...
                         </label>
                         <div class="fileInfo" style="display: none;">
                             <small id="fileName" class="text-center"></small><br/>
                             <small id="fileSize" class="text-center"></small><br/>
                             <small id="fileExtension" class="text-center"></small>
                        </div>
                        <form action="/ufirstassignment/core/uploadFile.php" id="uploadForm" class="file-upload" name="frmupload" method="post" enctype="multipart/form-data">
                            <input id="fileUpload" name="fileUpload" type="file">
                            <input id="submitButton" class="mt-2 btn btn-md btn-success btn-block rounded-pill" style="display: none;" type="submit" name='btnSubmit' value="Submit file" />
                        </form>
                        <div class="areaLoader" style="display: none;">
                            <div class="statusProcess"></div>
                        </div>
                      </div>
                   </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-lg-8">
            <div class="form-container pt-5 areaUploaderContainer">
                <div class="p-4 bg-white block-uploader">
                    <div class="row">
                        <?php 
                            $files = scan_dir();
                            $cont = 0;
                            $limit_cont = 2;

                            if(count($files) > 0 && $files) {
                                ?>
                                <div class="col-12">
                                    <small class="fw-600">Last files uploaded in JSON</small> - <small><a href="/ufirstassignment/assets/utils/epa-http.txt" target="_blank">Download main HTTP file</a> - <a href="javascript:void(0)" class="reloadFileList">Reload file list</a></small><br/>
                                </div>
                                <div class="col-12">
                                    <div class="row containerFiles">
                                    <?php
                                    foreach ($files as $file) {
                                        if($cont == $limit_cont) {
                                            break;
                                        }
                                        ?>
                                        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                            <a href="javascript:void(0)" data-filename="<?=$file?>" class="loadFile">
                                                <div class="file-grid">
                                                   <div class="file-thumnail">
                                                      <img src="/ufirstassignment/assets/img/json.png" class="w-img-json">
                                                   </div>
                                                   <div class="file-title"><?=$file?></div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                        $cont++;
                                    }
                                    ?>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-12">
                                    <small class="fw-600">No files found under uploads folder</small> - <small><a href="/ufirstassignment/assets/utils/epa-http.txt" target="_blank">Download main HTTP file</a> - <a href="javascript:void(0)" class="reloadFileList">Reload file list</a></small><br/>
                                </div>
                                <div class="col-12">
                                    <div class="row containerFiles"></div>
                                </div>
                                <?php
                            }
                        ?>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>