/* INITIALIZING REQUEST PER MINUTE CHART */
function drawChartRequestMinute(data_columns) {
    //Initializing the datatable object and adding some columns to it
    var data = new google.visualization.DataTable();
    data.addColumn('datetime', 'Time of Day');
    data.addColumn('number', 'Requests');
    rows = [];
    cont = 0;

    //Looping over the data_columns that have the server side information
    $.each(data_columns, function(index, column) {
        rows.push([new Date(2020, 5, column.day, column.hour, column.minute), parseInt(column.requests)]);
    });

    //Adding the rows array to the datatable
    data.addRows(rows);

    //Setting options
    var options = {
        hAxis: {
            title: 'Time'
        },
        vAxis: {
            title: 'Requests'
        },
        backgroundColor: '#fff',
        title: 'Requests per minute over the entire time span',
        legend: 'center',
        is3D: true
    };

    //Defininig the chart type and drawing it
    var chart = new google.visualization.LineChart(document.getElementById('chart_div_request_minute'));
    chart.draw(data, options);
}

function initializeChartRequestMinute(data_column) {
    //We initialize the charts before all the logic. We send the data to the function
    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(function() {drawChartRequestMinute(data_column)});
}
/* END REQUEST PER MINUTE CHART */

/* INITIALIZING HTTP METHODS CHART */
function drawChartHttpMethods(data_columns) {
    rows = [];
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Http Method');
    data.addColumn('number', 'Requests');

    //Looping over the data_columns that have the server side information
    $.each(data_columns, function(index, column) {
        rows.push([index, parseInt(column)]);
    });

    //Adding the rows array to the datatable
    data.addRows(rows);

    //Setting options
    var options = {
        title: 'HTTP Distribution Methods',
        colors: ['green'],
        histogram: { lastBucketPercentile: 5 },
    };

    //Initializing the datatable object and adding some columns to it
    var chart = new google.visualization.Histogram(document.getElementById('chart_div_http_methods'));
    chart.draw(data, options);
}

function initializeChartHttpMethods(data_column) {
    //We initialize the charts before all the logic.
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(function() {drawChartHttpMethods(data_column)});
}
/* END HTTP METHODS CHART */

/* INITIALIZING HTTP ANSWER CODES CHART */
function drawChartHttpAnswerCodes(data_columns) {
    rows = [];

    //Initializing the datatable object and adding some columns to it
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Http Method');
    data.addColumn('number', 'Requests');

    //Looping over the data_columns that have the server side information
    $.each(data_columns, function(index, column) {
        rows.push([index, parseInt(column)]);
    });

    //Adding the rows array to the datatable
    data.addRows(rows);

    //Setting options
    var options = {
        title: 'HTTP Answer Codes',
        histogram: { lastBucketPercentile: 10,  bucketSize: 3500},
        colors: ['#e7711c']
    };

    //Initializing the datatable object and adding some columns to it
    var chart = new google.visualization.Histogram(document.getElementById('chart_div_http_answer_methods'));
    chart.draw(data, options);
}

function initializeChartHttpAnswerCodes(data_column) {
    //We initialize the charts before all the logic.
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(function() {drawChartHttpAnswerCodes(data_column)});
}
/* END HTTP ANSWER CODES CHART */

/* INITIALIZING HTTP SIZE ANSWER CHART */
function drawChartHttpSizeAnswer(data_columns) {
    rows = [];

    //Initializing the datatable object and adding some columns to it
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Requests');

    //Looping over the data_columns that have the server side information
    $.each(data_columns, function(index, column) {
        rows.push([parseInt(index)]);
    });

    //Adding the rows array to the datatable
    data.addRows(rows);

    //Setting options
    var options = {
        title: 'Size answer',
        histogram: { lastBucketPercentile: 5 },
    };

    //Initializing the datatable object and adding some columns to it
    var chart = new google.visualization.Histogram(document.getElementById('chart_div_size_answer'));
    chart.draw(data, options);
}

function initializeChartSizeAnswer(data_column) {
    //We initialize the charts before all the logic.
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(function() {drawChartHttpSizeAnswer(data_column)});
}
/* END HTTP SIZE ANSWER CHART */