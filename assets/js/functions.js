/* MAIN JS FUNCTIONS*/
$(document).ready(function() {
    
    /*-------------------------*/
    /*                         */
    /* EVENT HANDLER FUNCTIONS */
    /*                         */
    /*-------------------------*/

    //Detecting if the input vale has a change
    $('#fileUpload').on("change", function(){
        var file = this.files[0];
        $('.areaLoader').show();
        $('#submitButton').show();
        $('.fileInfo').show();
        $('.statusProcess').html('');

        //It get's the input file name to add it to a element
        var filename = $('#fileUpload').val().split('\\').pop();
        $('#fileName').html('Filename: '+filename);

        //Showing the submit button
        $('#submitButton').show();

        //We have an array of sizes and we get the file input size. Later we add this information to an element.
        var _size = file.size;
        var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        i=0;while(_size>900){_size/=1024;i++;}
        var exactSize = (Math.round(_size*100)/100)+' '+fSExt[i];
        $('#fileSize').html('Filesize: '+exactSize)

        //We get the file extension to add it to an element
        $('#fileExtension').html('Extension: '+file.type);
    });

    $('body').on('click','.loadFile',function(){
        fileName = $(this).data('filename');
        $('#chart_div_request_minute').html('');
        $('#chart_div_http_methods').html('');
        $('#chart_div_http_answer_methods').html('');
        $('#chart_div_size_answer').html('');
        $('.chartContent').hide();

        $.ajax({
            url: '/ufirstassignment/core/loadFile.php',
            type: 'post',
            data: {
                file: fileName
            },
            complete: function (hxr) {
                charts_data = JSON.parse(hxr.responseText);
                $('.chartContent').show();
                
                if(charts_data.requestPerMinute != null) {
                    $('.blockChartRequestMinute').show();
                    initializeChartRequestMinute(charts_data.requestPerMinute);
                }

                if(charts_data.distributionHttpMethods != null) {
                    $('.blockChartHttpMethods').show();
                    initializeChartHttpMethods(charts_data.distributionHttpMethods);
                }

                if(charts_data.distributionHttpAnswerCodes != null) {
                    $('.blockChartHttpAnswerMethods').show();
                    initializeChartHttpAnswerCodes(charts_data.distributionHttpAnswerCodes);    
                }

                if(charts_data.distributionSizeAnswer != null) {
                    $('.blockChartSizeAnswer').show();
                    initializeChartSizeAnswer(charts_data.distributionSizeAnswer);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    });

    $('.reloadFileList').click(function() {
        $.ajax({
            url: '/ufirstassignment/core/loadFile.php',
            type: 'post',
            data: {
                reloadFiles: true
            },
            complete: function (xhr) {
                $('.containerFiles').html('');
                filesResponse = JSON.parse(xhr.responseText);
                console.log(filesResponse.files.length);
                if(filesResponse.files.length > 0) {
                    $.each(filesResponse.files, function(index, file) {
                        $('.containerFiles').append('<div class="col-12 col-sm-6 col-md-6 col-lg-6"> <a href="javascript:void(0)" data-filename="'+file+'" class="loadFile"> <div class="file-grid"> <div class="file-thumnail"> <img src="/ufirstassignment/assets/img/json.png" class="w-img-json"> </div> <div class="file-title">'+file+'</div> </div> </a> </div>');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
    });

    //Event handler for submit button
    $('#submitButton').click(function() {
        $('.areaLoader').show();
        
        $('#uploadForm').ajaxForm({
            target: '#outputImage',
            url: '/ufirstassignment/core/uploadFile.php',

            //Before submit we check if the input's value is empty
            beforeSubmit: function() {
                if ($("#fileUpload").val() == "") {
                    $(".statusProcess").html("<div class='error'>Choose a file to upload.</div>");
                    return false;
                }

                // get the file name, possibly with path (depends on browser)
                var filename = $("#fileUpload").val();

                // Use a regular expression to trim everything before final dot
                var extension = filename.replace(/^.*\./, '');

                // Iff there is no dot anywhere in filename, we would have extension == filename,
                // so we account for this possibility now
                if (extension == filename) {
                    extension = '';
                } else {
                    // if there is an extension, we convert to lower case
                    // (N.B. this conversion will not effect the value of the extension
                    // on the file upload.)
                    extension = extension.toLowerCase();
                }

                switch (extension) {
                    case 'txt':
                        $('.chartContent').hide();
                        break;

                    default:
                        // Cancel the form submission
                        $(".statusProcess").html("<div class='error'>Invalid extension. Only txt files.</div>");
                        return false;
                }
            },

            error: function(xhr) {
                $(".statusProcess").html(xhr.responseText.status);
            },

            complete: function(xhr) {
                response = JSON.parse(xhr.responseText);
                if (xhr.responseText && response.status != "error") {
                    $(".statusProcess").html('<i class="far fa-check-circle"></i> File uploaded and converted to JSON successfully. Please wait, we are loading the charts...');
                    $.ajax({
                        url: '/ufirstassignment/core/loadFile.php',
                        type: 'post',
                        data: {
                            file: response.filename
                        },
                        complete: function (hxr) {
                            $('.fileInfo').hide();
                            $('#fileName').html('');
                            $('#fileSize').html('');
                            $('#fileExtension').html('');
                            $('.areaLoader').hide();
                            $('#submitButton').hide();
                            $('#chart_div_request_minute').html('');
                            $('#chart_div_http_methods').html('');
                            $('#chart_div_http_answer_methods').html('');
                            $('#chart_div_size_answer').html('');
                            
                            charts_data = JSON.parse(hxr.responseText);

                            $('.chartContent').show();
                            
                            if(charts_data.requestPerMinute != null) {
                                $('.blockChartRequestMinute').show();
                                initializeChartRequestMinute(charts_data.requestPerMinute);
                            }

                            if(charts_data.distributionHttpMethods != null) {
                                $('.blockChartHttpMethods').show();
                                initializeChartHttpMethods(charts_data.distributionHttpMethods);
                            }

                            if(charts_data.distributionHttpAnswerCodes != null) {
                                $('.blockChartHttpAnswerMethods').show();
                                initializeChartHttpAnswerCodes(charts_data.distributionHttpAnswerCodes);    
                            }

                            if(charts_data.distributionSizeAnswer != null) {
                                $('.blockChartSizeAnswer').show();
                                initializeChartSizeAnswer(charts_data.distributionSizeAnswer);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                           console.log(textStatus, errorThrown);
                        }
                    });

                } else {
                    $(".statusProcess").html("<div class='error'>Problem in uploading file.</div>");
                }
            },
        });
    });

    /*-----------------------------*/
    /*                             */
    /* END EVENT HANDLER FUNCTIONS */
    /*                             */
    /*-----------------------------*/
});