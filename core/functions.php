<?php

//Function to scan the upload directory and return an array of files ordered by upload date
function scan_dir() {
	$dir = '/var/www/html/ufirstassignment/assets/uploads';

    //Ignoring all the dots generated by scandir
    $ignored = array('.', '..');
    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : false;
}

//Function that generates a random string, used to give the file an unique name.
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

//Function that explode the line received as param and format it
function explodeLine($line) {
    //Initializing variables for each loop iteration
    $host = '';
    $date_time = '';
    $request = '';
    $response_code = '';
    $document_size = '';

    //Exploding the line
    $explode_line = str_replace('"', '', $line);
    $explode_line = explode(" ", $explode_line);

    //Check if the host array element is set
    if(isset($explode_line[0])) {
        $host = $explode_line[0];
    }

    //Check if the datetime array element is set
    if(isset($explode_line[1])) {
        //We explode the element with the delimiter
        $date_time_exploded = explode(':', $explode_line[1]);

        //We check if the delimiter worked...
        if(isset($date_time_exploded)) {
            $date_time = [
                'day'       => str_replace('[', '', $date_time_exploded[0]),
                'hour'      => $date_time_exploded[1],
                'minute'    => $date_time_exploded[2],
                'second'    => str_replace(']', '', $date_time_exploded[3])
            ];
        }
    }

    //Check if the method is setted. 
    if(isset($explode_line[2])) {
        $http_methods = ['GET' ,'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE'];
        if(in_array($explode_line[2], $http_methods)) {
            $method = $explode_line[2];
        } else {
            return;
        }
    }

    //Check if the url is setted. 
    if(isset($explode_line[3])) {
        $url = $explode_line[3];
    }

    //Check if the protocol and the protocol_version
    if(isset($explode_line[4])) {
        $protocol = '';
        $protocol_version = '';

        //We replace the extra chars
        $formatted_protocol_n_version = str_replace('"', "", $explode_line[4]);
        
        //We explode the string with a slash
        $explode_protocol_n_version = explode("/", $formatted_protocol_n_version);

        //Checking if the protocol element is setted
        if(isset($explode_protocol_n_version[0])) {
            $protocol = $explode_protocol_n_version[0];
        }

        //Checking if the protocol version element is setted
        if(isset($explode_protocol_n_version[1])) {
            $protocol_version = $explode_protocol_n_version[1];
        }

        $request = [
            'method'              => $method,
            'url'                 => $url,
            'protocol'            => $protocol,
            'protocol_version'    => $protocol_version
        ];
    }

    //Checking if the response code is setted
    if(isset($explode_line[5])) {
        $http_codes = array(100, 101, 102, 103, 200, 201, 202, 203, 204, 205, 206, 207, 300, 301, 302, 303, 304, 305, 306, 307, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 422, 423, 424, 425, 426, 449, 450, 500, 501, 502, 503, 504, 505, 506, 507, 509, 510);
        if(in_array($explode_line[5], $http_codes)) {
            $response_code = $explode_line[5];
        } else {
            return;
        }
    }

    //Checking if the document size is setted
    if(isset($explode_line[6])) {
        $document_size = $explode_line[6];
    }

    //Formatting the final array
    $final_array = [
        'host'              => $host,
        'datetime'          => $date_time,
        'request'           => $request,
        'response_code'     => $response_code,
        'document_size'     => $document_size
    ];

    return $final_array;
}

function generateJsonFile($filename) {
    // We add each line as new position into the array.
    $lines = file($filename, FILE_IGNORE_NEW_LINES);

    // Main json_file that will be stored as file
    $array_lines_formatted = [];

    // Line processing for exploding and add the items into the json array
    foreach ($lines as $line) {
        //We send the line to the function for the processing
        $explode_line = explodeLine($line);

        //Checking if the array returned is setted
        if(isset($explode_line)) {
            $array_lines_formatted[] = $explode_line;
        }
    }

    return json_encode($array_lines_formatted);
}


function processRequestPerMinute($lines) {
    $chart_data = [];
    $chart_data_final = [];
    //We iterate over all the request and check if the day and the minute exists into the array. If exists we increment in 1 the request in the minute.
    foreach ($lines as $request) {
        if(!isset($chart_data[$request->datetime->day])) {
            $chart_data[$request->datetime->day][$request->datetime->hour][$request->datetime->minute] = 1;
        } else {
            $chart_data[$request->datetime->day][$request->datetime->hour][$request->datetime->minute] += 1; 
        }
    }

    //Once registered, we iterate again to structure it well for the chart processing
    foreach ($chart_data as $i_day => $day) {
        foreach ($day as $i_hour => $hour) {
            foreach ($hour as $i_minute => $minute) {
                $chart_data_final[] = ['day' => $i_day, 'hour' => $i_hour, 'minute' => $i_minute, 'requests' => $minute];
            }
        }
    }

    //We return de array
    return $chart_data_final;
}

function processDistributionHttpMethods($lines) {
    $chart_data = [];

    //We iterate over all the Http Methods and check if the method exists into the array. If exists we increment in 1 the request inside the method.
    foreach ($lines as $request) {
        if(!isset($chart_data[$request->request->method])) {
            $chart_data[$request->request->method] = 1;
        } else {
            $chart_data[$request->request->method] += 1;
        }
    }

    //We return de array
    return $chart_data;
}

function processDistributionHttpAnswerCodes($lines) {
    //We iterate over all the Http Answer Codes and check if the code exists into the array. If exists we increment in 1 the request inside the response code.

    $chart_data = [];
    foreach ($lines as $request) {
        if(!isset($chart_data[$request->response_code])) {
            $chart_data[$request->response_code] = 1;
        } else {
            $chart_data[$request->response_code] += 1;
        }
    }

    //We return de array
    return $chart_data;
}


function processDistributionSizeAnswer($lines) {
    $chart_data = [];
    foreach ($lines as $request) {
        //We iterate all over the lines to only add the requests with code 200 and the document size under 1000B. Later, we increment in 1 the quantity of times.
        if($request->response_code == 200 && $request->document_size < 1000) {
            if(!isset($chart_data[$request->document_size])) {
                $chart_data[$request->document_size] = 1;
            } else {
                $chart_data[$request->document_size] += 1;
            }

        }
    }

    //We return de array
    return $chart_data;
}
