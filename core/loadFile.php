<?php
	include('functions.php');

	//First of all we check for the file submission via form or via curl
	if(isset($_POST['file'])) {
		$file = $_POST['file'];
		$folder = '/var/www/html/ufirstassignment/assets/uploads';

		//If the file dont exists, we send a json response with the error
		if (!file_exists($folder.'/'.$file)) {
            echo json_encode(['status' => 'error']);
            exit();
		} else {
			//First of all, we load the content of the json file
			$json_data = file_get_contents($folder.'/'.$file);

			//Later, we decode it
			$json_data_decoded = json_decode($json_data);
			$array_charts = [];

			//We set the requestPerMinute array position into the array_charts
			$array_charts['requestPerMinute'] = processRequestPerMinute($json_data_decoded);
			$array_charts['distributionHttpMethods'] = processDistributionHttpMethods($json_data_decoded);
			$array_charts['distributionHttpAnswerCodes'] = processDistributionHttpAnswerCodes($json_data_decoded);
			$array_charts['distributionSizeAnswer'] = processDistributionSizeAnswer($json_data_decoded);

			//We return the array_charts
			echo json_encode($array_charts);
			exit();
		}
	}

	if(isset($_POST['reloadFiles'])) {
		$files = scan_dir();
        if(count($files) > 0 && $files) {
        	echo json_encode(['status' => 'success', 'files' => array_slice($files, 0, 2)]);
        	exit();
        } else {
        	echo json_encode(['status' => 'success', 'files' => []]);
        	exit();
        }
	}
?>