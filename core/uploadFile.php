<?php
    include('functions.php');
    
    //First of all we check if the form is submitted
    if (isset($_FILES["fileUpload"])) {
        if (!is_dir('../assets/uploads')) {
            mkdir('../assets/uploads', 0777, true);
        }

        $folderPath = "../assets/uploads/";
        
        // We check if the directory is writable or if is a dir
        if (!is_writable($folderPath) || !is_dir($folderPath)) {
            echo json_encode(['status' => 'error']);
            exit();
        }

        //We store into a variable the filename.
        $fileName = generateRandomString(25).'.'.pathinfo($_FILES["fileUpload"]["name"], PATHINFO_EXTENSION);

        //We attempt to upload the file to the directory and send it to the generateJsonFile function
        if (move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $folderPath.$fileName)) {
            //We call the main functions that do the refactoring for each line to an understable array
            $json_file = generateJsonFile($folderPath.$fileName);

            //We set the json_file_name
            $json_file_name = generateRandomString(25).'.json';

            //We open the directory and save a new file with the content
            $fp = fopen($folderPath.$json_file_name, "wb");
            fwrite($fp, $json_file);
            fclose($fp);

            //We delete the first file
            unlink($folderPath.$fileName);

            echo json_encode(['status' => 'success', 'filename' => $json_file_name]);
            exit();
        }
    }
?>