# UFirst Group Assignment

Your task is to import the access logs for the EPA from 1995, restructure the data and provide a graphical analysis of the data.
  - Write a script that imports the access logfile and creates a new file that holds the log data structured as a JSON-Array. (See the example at the bottom of this page)
  - Create one or more HTML- and Javascript-Files that read the JSON-File and render the following analysis graphically as charts:
    - Requests per minute over the entire time span
    - Distribution of HTTP methods (GET, POST, HEAD,...)
    - Distribution of HTTP answer codes (200, 404, 302,...)
    - Distribution of the size of the answer of all requests with code 200 and size < 1000B

# Used technologies

  - PHP
  - HTML
  - CSS
  - Javascript
  - jQuery
  - AJAX

You can also:
  - Import txt file
  - Select a uploaded file
  - Upload a file via curl

# cURL use
You can upload the file via command line in the following format:
```sh
$ curl -i -F fileUpload=@yourLocalFile http://51.178.43.68/ufirstassignment/core/uploadFile.php
```
Once you uploaded the file, you can click the "Refresh files" link inside the file container to get the last files.

See my webpage for more information about me [jsafont.com](https://jsafont.com)